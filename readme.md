# NOTE
## Package for laravel 5.1 & 5.2
_____ 5.1 _____
This packages using "illuminate/html", so please install it before using this package
1. How to use "illuminate/html" at: https://packagist.org/packages/illuminate/html
_____ 5.2 _____
This packages using "laravelcollective/html" instead, so please install it before using this package
1. How to use "laravelcollective/html" at: https://packagist.org/packages/laravelcollective/html

========== After that ==========

2. You need to add this service provider to: config/app.php
> "Gralias\Translates\TranslateServiceProvider::class"
3. And publish migrations and router with:
> "php artisan vendor:publish"
4. Remeber to migrate database (>.<) by typing: php artisan migrate
4. Test packages at
> yourdomain/translates
_____
How to use:
to generate locale language you must follow this:
- In view file:
```php
{!! trans(foo.bar_text_1) !!}
```
  - 'foo': name of file will save to : *resource/lang/en/foo.php.*
  - 'bar_text_1': string will be store in file like that
  ```php
    return ['bar_text_1' => 'bar text 1'];
  ```
- after set some strings in views you must update, translate and export it. Done!


Video introduce:
[![GRALIAS](http://img.youtube.com/vi/pBXuUgRt61U/0.jpg)](http://www.youtube.com/watch?v=pBXuUgRt61U)
