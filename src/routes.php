<?php
Route::get('translates/','Gralias\Translates\Controllers\TranslatesController@index');

Route::get('translates/importToDatabase',[
    'uses' => 'Gralias\Translates\Controllers\TranslatesController@importToDatabase',
    'as' => 'translates.update',
]);
Route::get('translates/exportToFile',[
    'uses' => 'Gralias\Translates\Controllers\TranslatesController@exportToFile',
    'as' => 'translates.export',
]);
Route::post('translates/ajaxChangeLanguage',[
        'uses' =>'Gralias\Translates\Controllers\TranslatesController@ajaxChangeLanguage',
        'as'  => 'translates.ajaxChangeLanguage'
    ]
);
Route::post('/translates/ajaxChangeLanguageDestroy',[
    'as' => 'translates.ajaxChangeLanguageDestroy',
    'uses' => 'Gralias\Translates\Controllers\TranslatesController@ajaxChangeLanguageDestroy'
]);
