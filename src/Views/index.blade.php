<html>
<head>
    <title>Multi language export tool</title>
    <meta name="csrf_token" content="{{ csrf_token() }}">
    <meta name="description" content="Scan and export language for multi-language project">
    <meta name="author" content="Gralias">
    <meta charset="UTF-8">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/1.4.0/css/flag-icon.min.css">
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
</head>

<body class="grey lighten-5">
<div class="container">
    <table class="striped bordered centered responsive-table">
        <thead>
            <tr>
                <th width="20%">Key</th>
                <th><span class="flag-icon flag-icon-us"></span></th>
                <th><span class="flag-icon flag-icon-vn"></span></th>
                <th><span class="flag-icon flag-icon-jp"></span></th>
                <th width="10%">Action</th>
            </tr>
            <tr>
                {!! Form::open(['method'=>'GET','autocomplete' => 'off']) !!}
                    <td>
                        <div class="input-field col s6">
                            {!! Form::text('key',Input::get('key'),['placeholder'=>'Key', ]) !!}
                        </div>
                    </td>
                    <td>
                        <div class="input-field col s6">
                            {!! Form::text('eng',Input::get('eng'),['placeholder'=>'English']) !!}
                        </div>
                    </td>
                    <td>
                        <div class="input-field col s6">
                            {!! Form::text('vie',Input::get('vie'),['placeholder'=>'Vietnamese']) !!}
                        </div>
                    </td>
                    <td>
                        <div class="input-field col s6">
                            {!! Form::text('jpn',Input::get('jpn'),['placeholder'=>'Japanese']) !!}
                        </div>
                    </td>
                    <td class='text-center'>
                        <button class="btn-floating green waves-effect waves-light "><i class="fa fa-search"></i></button>
                        <a href="{!! URL::to('translates/importToDatabase') !!}" class="btn-floating red waves-effect waves-light ">
                            <i class="fa fa-history"></i>
                        </a>
                    </td>
                {!! Form::close() !!}
            </tr>
        </thead>

        <tbody>
        <?php if(count($data) == 0): ?>
            <tr>
                <td colspan="5">{!! trans('default.No_data_to_display') !!}</td>
            </tr>
        <?php endif; ?>
        @foreach($data as $row)
            <tr>
                {!! Form::open(['method'=>'POST','route' => ['translates.ajaxChangeLanguage','id' => str_random(5).'_'.$row->id]]) !!}
                    {!! Form::hidden('id',$row->id) !!}
                    <td><?= explode('.',$row->key)[1]; ?></td>
                    <td>
                        <div class="input-field col s6">
                            {!! Form::text('eng',$row->eng) !!}
                        </div>
                    </td>
                    <td>
                        <div class="input-field col s6">
                            {!! Form::text('vie',$row->vie) !!}
                        </div>
                    </td>
                    <td>
                        <div class="input-field col s6">
                            {!! Form::text('jpn',$row->jpn) !!}
                        </div>
                    </td>
                    <td>
                        <button class="btn-floating green waves-effect waves-light ajaxLanguageSubmit" type='button'>
                            <i class="material-icons tiny">mode_edit</i>
                        </button>
                        <button class="btn-floating red waves-effect waves-light ajaxLanguageDestroy" type='button'>
                            <i class="fa fa-trash"></i>
                        </button>
                    </td>
                {!! Form::close() !!}
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="divider"></div>
    <div class="section">
        <div class="row">
            <div class="col s9">Page {!! $data->currentPage() !!}/{!! $data->lastPage() !!} of {!! $data->total() !!} records</div>
            <div class="col s3">
                <a class="btn-floating green {!! ($data->currentPage()==1) ? 'disabled' : '' !!}" href="{!! $data->url(1) !!}"><i class="fa fa-angle-double-left"></i></a>
                <a class="btn-floating green {!! ($data->currentPage()==1) ? 'disabled' : '' !!}" href="{!! $data->previousPageUrl() !!}"><i class="fa fa-angle-left"></i></a>
                <a class="btn-floating green {!! ($data->hasMorePages()) ? '' : 'disabled' !!}" href="{!! $data->nextPageUrl() !!}"><i class="fa fa-angle-right"></i></a>
                <a class="btn-floating green {!! ($data->hasMorePages()) ? '' : 'disabled' !!}" href="{!! $data->url($data->lastPage()) !!}"><i class="fa fa-angle-double-right"></i></a>
            </div>
        </div>
    </div>
</div>

<div class="fixed-action-btn horizontal" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large red">
        <i class="large material-icons tiny">toc</i>
    </a>
    <ul>
        <li><a class="btn-floating green tiny tooltipped" data-position="top" data-delay="50" data-tooltip="{!! trans('default.update_string_language') !!}" href="{!! URL::to('translates/importToDatabase') !!}"><i class="material-icons">loop</i></a></li>
        <li><a class="btn-floating blue darken-1 tiny tooltipped" data-position="top" data-delay="50" data-tooltip="{!! trans('default.export_string_language') !!}" href="{!! URL::to('translates/exportToFile') !!}"><i class="fa fa-download"></i></a></li>
    </ul>
</div>


</body>
</html>
<style>
    .toast {
        border:1px solid #fff;
    }
</style>
<script>
    $('.ajaxLanguageSubmit').click(function(){
        var activeForm = $(this.form);
        var urlForm    = activeForm.attr('action');
        var formValues = activeForm.serializeArray();
        var data = {};
        $.each(formValues,function(index,value){
            data[value.name] = value.value;
        });
        $.ajax({
            url: urlForm,
            data: data,
            method: 'POST',
            beforeSend: function(){
            },
            success: function(response){
                Materialize.toast('Success', 4000)
            },
            error: function(response){
                Materialize.toast('Failure', 4000)
            }
        });
    });
    $('.ajaxLanguageDestroy').click(function(){
        var activeForm = $(this.form);
        var trElement = $(this).parents('tr');
        var urlForm    = activeForm.attr('action');
        var formValues = activeForm.serializeArray();
        var data = {};
        data['destroy'] = true;
        $.each(formValues,function(index,value){
            data[value.name] = value.value;
        });
        $.ajax({
            url: urlForm,
            data: data,
            _token: $('meta[name="_token"]').attr('content'),
            type: 'POST',
            beforeSend: function(){
            },
            success: function(response){
                trElement.fadeOut();
                Materialize.toast('Success', 4000);
            },
            error: function(response){
                Materialize.toast('Failure', 4000)
            }
        });
    });
</script>
