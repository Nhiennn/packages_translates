<?php

namespace Gralias\Translates\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use Gralias\Translates\GraliasMultiLanguage;
use Illuminate\Support\Facades\Input;

class TranslatesController extends Controller
{
    public function index()
    {
        $key = (Input::has('key')) ? Input::get('key') : '';
        $eng = (Input::has('eng')) ? Input::get('eng') : '';
        $vie = (Input::has('vie')) ? Input::get('vie') : '';
        $jpn = (Input::has('jpn')) ? Input::get('jpn') : '';


        $data = GraliasMultiLanguage::where('key','like','%'.$key.'%')
            ->where('eng','like','%'.$eng.'%')
            ->where('vie','like','%'.$vie.'%')
            ->where('jpn','like','%'.$jpn.'%')
            ->paginate(8);

        return view('translates::index',compact('data'));
    }

    public function importToDatabase(){
        $text = $this->getAllText($this->getAllFile());
        foreach($text as $name){
            $search = GraliasMultiLanguage::where(['key' => $name])->get();
            if($search->count() == 0) GraliasMultiLanguage::insert(['key' => $name]);
        }
        return redirect('translates/');
    }

    public function exportToFile(){
        $strings = GraliasMultiLanguage::get(['key','eng','vie','jpn'])->toArray();
        $resourcePath = base_path('resources/lang/');
        $arr = [];
        foreach($strings as $data){
            $file = explode('.',$data['key'])[0];
            $value = explode('.',$data['key'])[1];
            $arr['en'][$file][$value] = $data['eng'];
            $arr['vi'][$file][$value] = $data['vie'];
            $arr['jp'][$file][$value] = $data['jpn'];
        }

        foreach($arr as $lang => $file){
            if (!is_dir($resourcePath.$lang)){
                mkdir($resourcePath.$lang,0777,true);
                @chmod($resourcePath.$lang,0777);
            }
            foreach($file as $name => $value){
                $viewCreate = view('translates::template', [
                    'data' => $value
                ]);
                file_put_contents($resourcePath.$lang.'/'.$name.'.php',$viewCreate);
                @chmod($resourcePath.$lang.'/'.$name.'.php',0777);
            }
        }
        return redirect('translates/');
    }

    public function ajaxChangeLanguage(){
        $record = GraliasMultiLanguage::find(Input::get('id'));
        if(Input::get('destroy')){
            if($record->delete()){
                echo json_encode(['status' => true ]);
            } else {
                echo json_encode(['status' => false]);
            }
            die;
        }
        $record->eng = Input::get('eng');
        $record->vie = Input::get('vie');
        $record->jpn = Input::get('jpn');
        if($record->save()){
            echo json_encode(['status' => true]);
        } else {
            echo json_encode(['status' => false]);
        }
        die;
    }

    public function getAllText($fileList){
        $allKey = array();
        foreach($fileList as $file) {
            $ext = $this->_getFileExtension($file);
            if(!in_array($ext,array('php','ctp'))) continue;
            $content = file_get_contents($file);
            $pattern = "^trans\('(.*?)\'\)^";
            preg_match_all($pattern, $content, $matches);
            if(isset($matches[1])) {
                foreach($matches[1] as $match) {
                    array_push($allKey,$match);
                }
            }
        }
        $allKey = array_unique($allKey);
        return $allKey;
    }

    public function getAllFile(){
        $target = [
            'app',
            'resources',
            'packages',
        ];
        $files = [];

        foreach($target as $folder){
            if(is_dir(base_path().'/'.$folder.'/')){
                $files = array_merge($files,$this->filterFileType($this->_directoryToArray(base_path().'/'.$folder.'/')));
            }
        }
        return $files;
    }

    public function filterFileType($array){
        $newData = [];

        foreach($array as $file) {
            $ext = $this->_getFileExtension($file);
            if(!in_array($ext,array('php','ctp'))) continue;
            $newData[] = $file;
        }

        return $newData;
    }

    public function _directoryToArray($directory, $recursive=true)
    {
        $array_items = array();
        if ($handle = opendir($directory)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    if (is_dir($directory. "/" . $file)) {
                        if($recursive) {
                            $array_items = array_merge($array_items, $this->_directoryToArray($directory. "/" . $file, $recursive));
                        }
                        $file = $directory . "/" . $file;
                        $array_items[] = preg_replace("/\/\//si", "/", $file);
                    } else {
                        $file = $directory . "/" . $file;
                        $array_items[] = preg_replace("/\/\//si", "/", $file);
                    }
                }
            }
            closedir($handle);
        }
        return $array_items;
    }

    function _getFileExtension($str)
    {
        $i = strrpos($str,".");
        if (!$i) { return ""; }
        $l = strlen($str) - $i;
        $ext = substr($str,$i+1,$l);
        return $ext;
    }
}
